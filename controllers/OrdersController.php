<?php

namespace app\controllers;

use Yii;
use app\models\Orders;
use app\models\OrdersSearch;
use app\models\Dbqueue;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OrdersController implements the CRUD actions for Orders model.
 */
class OrdersController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Orders models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrdersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Orders model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Orders model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Orders();

        if ($model->load(Yii::$app->request->post())) {

            $model->mobile = "254" . substr($model->mobile, -9);
            $model->list = $model->list;
            $model->deliveryCost = (int) $this->formatNo($model->deliveryCost);
            $model->totalCost = (int) $this->formatNo($model->totalCost);
            $model->servedBy = $model->servedBy;
            //order no
            $length = 12;
            $characters = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $orderno = substr(str_shuffle($characters), 0, $length);
            $model->orderNumber = "TUS".$orderno;
            //otp
            $model->otp = rand(10000,99999);
            //ticket id
            $model->ticketId = Yii::$app->db->createCommand("SELECT `id` FROM `ticket` WHERE `mobile`= ".$model->mobile." AND `ticket_status`='Active'")
            ->execute();

            // var_dump($model->ticketId);
            // die();

            if($model->save()){

            $mobile = 14155238886;
            $state = "ticket";
            $message = "Dear customer, your shopping list is as follows: " . $model->list . ". Your Total Cost is Kshs.".$model->totalCost."(inclusive of your delivery cost Kshs.".$model->deliveryCost."). Kindly proceed to make your payment through our Till Number 100200.";
            $ticket = (new \yii\db\Query())
                ->select(['id'])
                ->from('ticket')
                ->where(['mobile' => $model->mobile])
                ->andwhere(['ticket_status'=>'Active'])
                ->one();

            $ticketid = $ticket['id'];
            $msg = (new \yii\db\Query())
                ->select(['id'])
                ->from('incoming')
                ->where(['mobile' => $model->mobile])
                ->one();
            $msg_id = $msg['id'];

            Yii::$app->db->createCommand("UPDATE ticket AS t LEFT JOIN `orders` AS o ON t.`id` = o.`ticketId` SET t.`ticket_status` = 'OrderCreated' WHERE t.`id` = ".$model->ticketId." AND t.`mobile`= ".$model->mobile." AND t.`ticket_status`='Active'")
            ->execute();

            Yii::$app->db->createCommand("INSERT INTO outgoing (mobile,msg,dest,ticket_id,state,msg_id) VALUES ('".$mobile."','".$message."','".$model->mobile."','".$ticketid."','".$state."','".$msg_id."') ")
            ->execute();

            Yii::$app->db->createCommand("UPDATE ticket SET `ticket_status` = 'OrderCreated' WHERE `mobile`= ".$model->mobile." AND `ticket_status`='Active'")
            ->execute();

            Yii::$app->session->setFlash('success', 'Shopping list for&nbsp;'.$model->mobile.' with order number&nbsp;'.$model->orderNumber.'&nbsp;has been successfully created.'); 


            return $this->redirect(['create']);
            }else{
                $model->mobile = "0" . substr($model->mobile, 3);
                $model->list ='';
                $model->deliveryCost='';
                $model->totalCost='';
                $model->servedBy='';
                //$user->msisdn = "254" . substr($this->msisdn, 1);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Orders model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

            $model->mobile = "254" . substr($model->mobile, -9);
            $model->list = $model->list;
            $model->deliveryCost = (int) $this->formatNo($model->deliveryCost);
            $model->totalCost = (int) $this->formatNo($model->totalCost);
            $model->servedBy = $model->servedBy;

            if($model->save()){

            $dbqueue = new Dbqueue();
            $dbqueue->originator = Dbqueue::SENDERID;
            $dbqueue->destination = $model->mobile;
            $dbqueue->message = "Dear customer, your shopping list has been updated as follows: " . $model->list . ". Your Total Cost is Kshs.".$model->totalCost."(inclusive of your delivery cost Kshs.".$model->deliveryCost."). Kindly proceed to make your payment through out Till Number 100200.";
            $dbqueue->save(); 

            return $this->redirect(['view', 'id' => $model->id]);
            }else{

                Yii::$app->session->setFlash('error', 'Shopping list has not been updated. Kindly try again.');

                return $this->redirect(['update']);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Orders model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function formatNo($value)
    {
      return preg_replace("/[^0-9]/", "", $value);
    }

    /**
     * Finds the Orders model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Orders the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Orders::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
