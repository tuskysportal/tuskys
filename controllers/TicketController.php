<?php

namespace app\controllers;

use Yii;
use app\models\Ticket;
use app\models\TicketSearch;
use app\models\Outgoing;
use app\models\TicketActiveSearch;
use app\models\TicketClosedSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TicketController implements the CRUD actions for Ticket model.
 */
class TicketController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Ticket models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TicketSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionIndexactive()
    {
        $searchModel = new TicketActiveSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('indexactive', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionIndexclosed()
    {
        $searchModel = new TicketClosedSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('indexclosed', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Ticket model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionViewdemo($id)
    {
        return $this->render('viewdemo', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionViewactivedemo($id)
    {
        return $this->render('viewactivedemo', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Ticket model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Ticket();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Ticket model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionUpdatedemo($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

            

            if($model->save()){

            $mobile = 14155238886;
            $destination = $model->mobile;
            $ticketid = $model->id;
            $state = "ticket";
            $status = "Pending";

            $msg = (new \yii\db\Query())
                ->select(['incoming.id'])
                ->from('ticket')
                ->join('LEFT JOIN', 'incoming', 'ticket.id = incoming.ticket_id')
                ->where(['ticket.mobile' => $model->mobile])
                ->andwhere(['incoming.ticket_id'=> $model->id])
                ->andwhere(['ticket.ticket_status'=>'Pending'])
                ->one();

            $msg_id = $msg['id'];
            
            Yii::$app->db->createCommand("INSERT INTO outgoing (mobile,msg,dest,ticket_id,state,msg_id) VALUES ('".$mobile."','".$model->reply."','".$model->mobile."','".$model->id."','".$state."','".$msg_id."') ")
            ->execute();

            Yii::$app->db->createCommand("UPDATE ticket SET `ticket_status` = 'Active' WHERE `id` = ".$model->id." AND `mobile`= ".$model->mobile." AND `ticket_status`='Pending'")
            ->execute();

            return $this->redirect(['viewdemo', 'id' => $model->id]);

            }else{

                Yii::$app->session->setFlash('error', 'Reply not successful. Kindly try resending again.');

                return $this->redirect(['updatedemo']);

            }
        }

        return $this->render('updatedemo', [
            'model' => $model,
        ]);
    }

    public function actionUpdateactivedemo($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

            

            if($model->save()){

            $mobile = 14155238886;
            $destination = $model->mobile;
            $ticketid = $model->id;
            $state = "ticket";
            $status = "Active";

            $msg = (new \yii\db\Query())
                ->select(['incoming.id'])
                ->from('ticket')
                ->join('LEFT JOIN', 'incoming', 'ticket.id = incoming.ticket_id')
                ->where(['ticket.mobile' => $model->mobile])
                ->andwhere(['incoming.ticket_id'=> $model->id])
                ->andwhere(['ticket.ticket_status'=>'Active'])
                ->one();

            $msg_id = $msg['id'];
            
            Yii::$app->db->createCommand("INSERT INTO outgoing (mobile,msg,dest,ticket_id,state,msg_id) VALUES ('".$mobile."','".$model->reply."','".$model->mobile."','".$model->id."','".$state."','".$msg_id."') ")
            ->execute();

            return $this->redirect(['viewactivedemo', 'id' => $model->id]);

            }else{

                Yii::$app->session->setFlash('error', 'Reply not successful. Kindly try resending again.');

                return $this->redirect(['updateactivedemo']);

            }
        }

        return $this->render('updateactivedemo', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Ticket model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Ticket model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Ticket the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Ticket::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionFetchIncomingMessage(){

        $model = new Ticket();

        $data = $model->incoming();
    }

    public function actionAjax12(){
        echo "we here";

        $model = new Ticket();

        $data = $model->outgoing();
    }

    // public function actionAjax()
    // {
    //     $model = new Ticket();
    //     if ($model->load(Yii::$app->request->post())) {
    //           $data = Yii::$app->request->post();
    //           $msisdn = Yii::$app->user->username;
    //           $msisdn = "254" . substr($msisdn, -9);
    //           $searchModel = new TicketSearch();
    //           $dataProvider = $searchModel->searchajax($msisdn);
    //            return $this->render('index', [
    //           'searchModel' => $searchModel,
    //           'dataProvider' => $dataProvider,
    //       ]);

    //    } else {

    //         return $this->render('index1', [
    //             'model' => $model,
    //         ]);
    //     }

    //
    public function actionAjax()
    {

        $id  = isset($_POST['id']) ? $_POST['id'] : null;
        $msg = isset($_POST['msg']) ? $_POST['msg'] : null;
        $sql    = "select * from ticket where id=:id";
        $ticket = Yii::$app->db->createCommand($sql, [
             ':id' => (int) $id
        ])->queryOne();

        if(!$ticket)
        {
            return $this->asJson([
                'status' => false,
                'statusText' => '404 Content not found',
            ]);
        }

        if(!$msg)
        {
            return $this->asJson([
                'status' => false,
                'statusText' => 'The message cannot be empty',
            ]);
        }

        $model = new Outgoing();
        $model->mobile      = Yii::$app->user->identity->username;
        $model->dest        = '14155238886';
        $model->ticket_id   = $id;
        $model->state       = 'ticket';
        $model->msg         = \yii\helpers\Html::encode($msg);
        $model->created_at  = new \yii\db\Expression('NOW()');

        if($model->save() == false)
        {
            return $this->asJson([
                'status' => false,
                'statusText' => 'An error occurred while trying to save your data. please refresh your browser and try again',
                'errors'      => $model->getErrors(),
            ]);
        }

        $model->refresh();

        return $this->asJson([
            'status' => true,
            'item'   => [
                'id'         => $model->id,
                'ticket_id'  => $model->ticket_id,
                'mobile'     => $model->mobile,
                'IN'         => 'OUT',
                'msg'        => Yii::$app->formatter->asNtext($model->msg),
                'created_at' =>Yii::$app->formatter->asDatetime($model->created_at , 'short'),
            ],
        ]);
    }


    public function actionHistory($id)
    {
        $sql    = "select * from ticket where id=:id";
        $ticket = Yii::$app->db->createCommand($sql, [
             ':id' => (int) $id
        ])->queryOne();

        if(!$ticket)
        {
            return $this->asJson([
                'status' => false,
                'statusText' => '404 Content not found',
            ]);
        }

        $sql = '
            SELECT * FROM (SELECT `id`,`mobile`,`msg`,`ticket_id`,`created_at`,"IN"
            FROM `incoming`
            WHERE ticket_id = :id
            UNION
            SELECT `id`,`mobile`,`msg` AS reply,`ticket_id`,`created_at`,"OUT"
            FROM `outgoing`
            WHERE ticket_id = :id) AS conversation ORDER BY `created_at` asc ;
        ';

        $out = [];
        foreach(Yii::$app->db->createCommand($sql, [':id' => (int) $id])->queryAll() as $data)
        {
            $data['msg']         = Yii::$app->formatter->asNtext( $data['msg'] );
            $data['created_at']  = Yii::$app->formatter->asDatetime( $data['created_at'] , 'short');
            array_push($out, $data);
        }

        return $this->asJson([
            'status' => true,
            'ticket' => $ticket,
            'items'  => $out,

        ]);
    }
}


