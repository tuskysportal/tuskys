$.fn.chat = function(options){

	const ID_TICKET = $(this).data('id') || false;

	var gate = $(this);
	gate.blur();

	if(gate.attr('disabled') )
		return;
	else
		this.attr('disabled' , 'disabled');

	var   textArea         = null;  // node textarea
	var   btnSubmit        = null;  // node btn submit

	var setup = {
		title              : 'WhatsApp Chat',    
		iconClass          : 'fa fa-users',      
		sizeClass          : 'modal-lg',         
		routes : {
			add           : '',                  
			history       : '',                  
		}
	};

	$.extend(setup, options);

	var modal = (function(){
		var obj = new domModal();
			obj.registerEvent('beforeOpen', function(){
				$('.modal').modal('hide');
			});

			obj.registerEvent('afterOpen', function(){
				loadHistory();
			});


			obj.registerEvent('afterClose', function(){
				$('.modal').modal('hide');
				obj.bsModal.remove();
				gate.removeAttr('disabled');
			});

			obj.setIcon(setup.iconClass);
			obj.setTitle(setup.title);
			obj.setSize(setup.sizeClass);
			return obj;
	})();


	// this function create unique rand id for elements (prevent collision of ids elements dom)
	function uniqid(){
		function rand(){
			  return "xxxx_xxxx_xxxx_xxxx_xxxx".replace(/[xy]/g, function(c) {
				var r = Math.random() * 16 | 0, v = c == "x" ? r : (r & 0x3 | 0x8);
				return v.toString(16);
			  });
		}
		return "node_" + rand();
	}

	// this function create modal dynamic
	function domModal(){
		var obj       = {};
		var _icon     = $('<i>');
		var _title    = $('<span>');

		// set properties:
		(function(){
			obj.header = (function(){
				var e = $('<div>')

				e.btnClose = $('<button>').attr({
					'type'  : 'button',
					'class' : 'close',
					'data-dismiss' : 'modal',
				}).html('&times;');

				(function(){
					e.append(e.btnClose);

					$('<h4>')
						.addClass('modal-title')
						.append(_icon)
						.append(_title)
						.appendTo(e);
				})();

				return e;
			})();

			obj.body = (function(){
				var e = $('<div>')
				return e;
			})();

			obj.footer = (function(){

				var e = $('<div>')
					.addClass('modal-footer')

				textArea = $('<textarea>')
					.addClass('form-control')
					.attr('placeholder' , 'Add message')

				btnSubmit = $('<button>')
					.addClass('btn btn-primary')
					.html('<i class="fa fa-paper-plane"></i>')
					.on('click', function(){
						newMassage();
					});

				var formContent = $('<div>')
					.addClass('formContent clearfix')
					.append(btnSubmit)
					.append(textArea)
					.appendTo(e);
				return e;
			})();

			obj.content = (function(){

				var e = $('<div>')
					.append(obj.header)
					.append(obj.body)
					.append(obj.footer);

				return e;
			})();

			obj.dialog = (function(){
				var e = $('<div>')
					.append(obj.content);
				return e;
			})();

			obj.bsModal = (function(){
				var e = $('<div>')
					.attr('id',   uniqid())
					.attr('role', 'dialog')
					.html(obj.dialog)
				return e;
			})();
		})();

		// methods
		(function(){
			obj.setIcon = function(iconClass){
				_icon.attr('class', iconClass);
			};

			obj.setTitle = function(title){
				_title.html(title)
			};

			obj.setSize = function(sizeClass){
				obj.dialog.attr('class',  'modal-dialog ' + (sizeClass || '') );
			}

			obj.open = function(){

				/// @todo check if is other model is open!
				if(obj.bsModal.is(':visible') == false)
				{
					$('body').prepend(obj.bsModal);
					obj.bsModal.modal({
						'backdrop' : 'static',
						'keyboard' : 'true',
					});
				}
			};

			obj.close = function(){
				if(obj.bsModal.is(':visible') == true)
					obj.bsModal.modal('hide');

				setTimeout(function(){
					obj.reset(false);
				}, 400);
			};

			obj.removeAllEvents = function(){
				obj.bsModal.off("show.bs.modal");
				obj.bsModal.off("shown.bs.modal");
				obj.bsModal.off("hide.bs.modal");
				obj.bsModal.off("hidden.bs.modal");
			};

			obj.reset = function(resetEvents){

				var resetEvents = resetEvents || true;

				if(obj.bsModal.is(':visible'))
					obj.bsModal.attr('class', 'modal modal-chat fade in');
				else
					obj.bsModal.attr('class', 'modal modal-chat fade');

				obj.dialog.attr('class',  'modal-dialog');
				obj.content.attr('class', 'modal-content');
				obj.header.attr('class', 'modal-header');
				obj.header.btnClose.attr('class', 'close');
				_icon.removeAttr('class');
				_title.removeAttr('class').html('');

				obj.body.attr('class','modal-body');
				obj.footer.attr('class','modal-footer');

				if(resetEvents == true)
					obj.removeAllEvents();
			};

			obj.registerEvent = function(eventName, fn){

				var _event = null;

				switch(eventName)
				{
					case 'show.bs.modal':
					case 'beforeOpen':
						_event = 'show.bs.modal';
						break;

					case 'shown.bs.modal':
					case 'afterOpen':
						_event = 'shown.bs.modal';
						break;

					case 'hide.bs.modal':
					case 'beforeClose':
						_event = 'hide.bs.modal';
						break;

					case 'hidden.bs.modal':
					case 'afterClose':
						_event = 'hidden.bs.modal';
						break;

					default:
						return;
				}

				if($.isFunction(fn) == false)
					return;

				obj.bsModal.off(_event);
				obj.bsModal.on(_event, fn);
				return;
			};
		})();

		obj.reset();
		return obj;
	}

	// add message in the modal-body
	function addMessage(data)
	{
		var msn = $('<div>')
			msn.addClass('msn ' + data.IN);
			msn.appendTo(modal.body);

		var header = $('<div>');
			header.addClass('msn-header clearfix');
			header.append('<div class="pull-left  font12"><b>' + data.mobile     +':</b></div>')
			header.append('<div class="pull-right font12"><b>' + data.created_at +'</b></div>')
			header.appendTo(msn);

		var msnContainer = $('<div>');
			msnContainer.addClass('msn-container');
			msnContainer.html('<p class="text-aling justify">'+ data.msg  +'</p>')
			msnContainer.appendTo(msn);

		$(modal.body).scrollTop( $(modal.body).get(0).scrollHeight );
	}

	function newMessage(){

		if(textArea.val() == '' || btnSubmit.attr('disabled'))
			return;

		setTimeout(function(){
			textArea.attr('disabled', 'disabled');
			btnSubmit.attr('disabled', 'disabled');
			btnSubmit.html('<i class="fa fa-circle-o-notch fa-spin"></i>');

			$.ajax({
				url      : setup.routes.add,
				type     : 'POST',
				dataType : 'JSON',
				data     : {
					'id'  : ID_TICKET,
					'msg' : textArea.val(),
				},
			})
			.done(function(resp){

				if(resp.status == false)
				{
					alert(resp.statusText);
					return;
				}

				addMessage(resp.item);
				textArea.val('');
			})
			.fail(function(data){
				//window.location.reload(true);
			})
			.always(function(){
				textArea.removeAttr('disabled');
				btnSubmit.removeAttr('disabled', 'disabled');
				btnSubmit.html('<i class="fa fa-paper-plane"></i>');
			});

		}, 255);
	}

	function loadHistory(){
		$.ajax({
			url      : setup.routes.history,
			type     : 'GET',
			data     : {'id' : ID_TICKET },
			dataType : 'JSON',
			cache    : false,
		})
		.done(function(resp){
			modal.body.html('');
			modal.body.removeClass('loader');

			if(resp.status == false)
			{
				modal.setIcon('fa fa-bug');
				modal.setTitle('Error');
				modal.header.fadeIn();
				modal.body.html('<div class="text-center text-danger" style="margin-top:100px"><h3>'+ resp.statusText +'</h3></div>');
				return;
			}
			modal.header.show();
			modal.footer.show();
			$(resp.items).each(function(k,v){
				addMessage(v, true);
			});

		})
		.fail(function(error){
			window.location.reload(true)
		});
	}

	// init
	(function(){
		modal.header.hide();
		modal.footer.hide();
		modal.body.addClass('loader');
		modal.open();
	})();
};


