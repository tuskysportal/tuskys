<?php
namespace app\assets\chat;

use Yii;
use yii\web\AssetBundle;

class ChatAsset extends AssetBundle
{
    public $basePath        = '@webroot';
    public $sourcePath      = __DIR__ . '/assets';
    public $css             = [];
    public $js              = [];
    public $depends         = [];

    public function init()
    {
        parent::init();
        //registers for each  ajax request. 
        if(Yii::$app->request->isAjax)
            return;

        $this->css = [
            'chat.css',
        ];

        $this->js = [
            'chat.js',
        ];

        $this->depends = [
            'yii\web\YiiAsset',
            'yii\bootstrap\BootstrapPluginAsset',
        ];
    }
}
