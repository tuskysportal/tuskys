<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Users', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'username:ntext',
            'firstname',
            'lastname',
            'msisdn',
            //'repeat_password',
            //'name:ntext',
            //'email:ntext',
            //'password:ntext',
            //'salt:ntext',
            //'activation_key:ntext',
            //'status_id',
            //'dateCreated:ntext',
            //'created_by',
            //'dateModified:ntext',
            //'modified_by',
            //'role_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
