<?php

use yii\helpers\Html;
use yii\helpers\Url;
use app\assets\AppAsset;
use kartik\grid\GridView;
use kartik\export\ExportMenu;
use app\models\Ticket;


use app\assets\chat\ChatAsset;
ChatAsset::register($this);

Yii::setAlias('@js', '@web/js/');


/* @var $this yii\web\View */
/* @var $searchModel app\models\TicketSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
AppAsset::register($this);
Yii::setAlias('@js', '@web/js/');


$this->title = 'Pending Tickets';
$this->params['breadcrumbs'][] = $this->title;
?>
<head>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
<style type="text/css">
    .panel-primary > .panel-heading {
    color: #fff;
    background-color: #465A6E;
    border-color: #465A6E;
    }
    .btn-primary {
        color: #fff;
        background-color: #465A6E;
        border-color: #465A6E;
        border-radius: 35px;
    }
    .pagination > .active > a, .pagination > .active > span, .pagination > .active > a:hover, .pagination > .active > span:hover, .pagination > .active > a:focus, .pagination > .active > span:focus {
        z-index: 3;
        color: #fff;
        cursor: default;
        background-color: #465A6E;
        border-color: #465A6E;
    }
    .red {
        color: white;
        background: red;
    }
    .btnstatus {
        border-radius: 35px;
        margin-bottom: 5px;
        margin-right: 5px;
        padding: 6px 12px;
        font-size: 14px;
        line-height: 1.42857143;
        touch-action: manipulation;
        cursor: pointer;
        background-image: none;
        border: 1px solid transparent;
        display: inline-block;
        font-weight: normal;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
    }
</style>
</head>
<div class="ticket-index">

    <h3 style="padding-bottom: 5px;margin-top: -10px"><?= Html::encode($this->title) ?></h3>
    <p>
    <?php echo ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            
            //'id',
            'mobile',
            'msg:ntext',
            'dest',
            'ticket_id',
            //'created_at',
            //'state',
            //'acc_id',
        ],
        
    ]); ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],

            // /'id',
            'mobile',
            [
            'label' => 'Message',
            'value' => function($data){
                return Ticket::get_user_message($data->mobile);
            },
            ],
            //'ticket_status',
            'date_created',
            [
            'attribute'=>'ticket_status',
            'header'=>'Ticket Status',
            'format'=>'raw',    
            'value' => function($model, $key, $index)
            {   
                    return '<button class="btnstatus red">Pending</button>';
            },
            ],
            //'user_id',
            [
                'header' => '',
                'format'    => 'raw',
                'value'     => function($model){

                    $text    = 'Zoom Live Chat';
                    $options = [
                        'class'        => 'btn btn-primary btn-block',
                        'data-runtime' => 'addTiket',
                        'title'        => $text,
                        'data-pjax'    => '0',
                        'data-id'      => $model->id,
                    ];
                    return Html::a($text,'javascript:void(0)', $options);
                },
            ],            
            [
                     'class' => 'yii\grid\ActionColumn',
                     'template' => '{updatedemo}',
                     'buttons' => [
                     'updatedemo' => function ($data, $key,$url) {
                        return Html::a('Reply (DEMO)',['updatedemo','id' => $key['id']], 
                        [ 'title' => 'Reply (DEMO)','data-pjax' => '0','class' => 'btn btn-primary',]
                        );
                        }
                     
                        ],
            ],
            [
                'class' => 'kartik\grid\ActionColumn',
                'template' => '{view}',
                'dropdown' => true,
                'vAlign'=>'middle',
            ],

        ],
        'containerOptions' => ['style'=>'overflow: auto'], 
        'toolbar' =>  [
            //'{export}',
            '{toggleData}'
        ],
        'pjax' => true,
        'bordered' => true,
        'striped' => false,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'floatHeader' => false,
        //'showPageSummary' => true,
        'rowOptions' => ['class' => ''],
        'headerRowOptions' => ['class' => ''],
        'layout' => "{items}\n{pager}",
        'panel' => [
            'type' => GridView::TYPE_PRIMARY
        ],
    ]); ?>


</div>

<?php
   $this->registerJs('
    $("a[data-runtime=addTiket]").on("click", function(event){
        event.preventDefault();
        event.stopPropagation();
        $(this).chat({
          "title":"WhatsApp Live Chat",
          "iconClass":"fa fa-whatsapp",
          "routes":{
            "add"     : "'. Url::toRoute(['/ticket/ajax'])     .'" ,
            "history" : "'. Url::toRoute(['/ticket/history'])  .'" ,
          }
        });
        return false;
    });
  ', $this::POS_END, 'ticket-msn');

