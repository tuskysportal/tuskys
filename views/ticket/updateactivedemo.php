<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Ticket */

$this->title = 'Reply To Ticket: ' . $model->mobile;
$this->params['breadcrumbs'][] = ['label' => 'Tickets', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ticket-update">

    <h3 style="padding-bottom: 5px;margin-top: -10px"><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_formactivedemo', [
        'model' => $model,
    ]) ?>

</div>
