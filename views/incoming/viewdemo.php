<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Incoming;
/* @var $this yii\web\View */
/* @var $model app\models\Ticket */

$this->title = $model->mobile;
$this->params['breadcrumbs'][] = ['label' => 'Tickets', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="ticket-view">

    <h3 style="padding-bottom: 5px;margin-top: -10px"><?= Html::encode($this->title) ?></h3>

    <p>
        <?= Html::a('Back To Active Tickets', ['index'], [
            'class' => 'btn btn-danger'
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'mobile',
            [
            'label' => 'Reply',
            'value' => function($data){
                return Incoming::get_active_cashier_reply($data->mobile,$data->id);
            },
            ],
        ],
    ]) ?>

</div>
