<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\date\DatePicker;
use kartik\datetime\DateTimePicker; 

/* @var $this yii\web\View */
/* @var $model app\models\Packaging */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="packaging-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'orderId')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Count_Of_Packages')->textInput() ?>

    <?= $form->field($model, 'packing_status')->dropDownList([ 'Not_Packed' => 'Not Packed', 'Packing_In_Progress' => 'Packing In Progress', 'Packing_Complete' => 'Packing Complete', 'Sent_To_Dispatch' => 'Sent To Dispatch', ], ['prompt' => '']) ?>

    <?= $form->field ( $model, 'packing_start_time', [ ] )->widget ( DateTimePicker::classname (), [ 
        'options' => [ 
            'placeholder' => 'Pick Packing Start Time',
        ],
        'value' => date ( 'Y-M-d H:i:s'),
        'type' => DatePicker::TYPE_COMPONENT_PREPEND,
        'readonly' => true,
        'pluginOptions' => [ 
            'autoclose' => true,
            'format' => 'yyyy-mm-dd HH:ii:ss',
            'todayHighlight' => TRUE 
        ] 
    ] )->label ( false );

    ?>
    <?= $form->field ( $model, 'packing_end_time', [ ] )->widget ( DateTimePicker::classname (), [ 
        'options' => [ 
            'placeholder' => 'Pick Packing End Time',
        ],
        'value' => date ( 'Y-M-d H:i:s'),
        'type' => DatePicker::TYPE_COMPONENT_PREPEND,
        'readonly' => true,
        'pluginOptions' => [ 
            'autoclose' => true,
            'format' => 'yyyy-mm-dd HH:ii:ss',
            'todayHighlight' => TRUE 
        ] 
    ] )->label ( false );

    ?>
    <?= $form->field ( $model, 'verification_time', [ ] )->widget ( DateTimePicker::classname (), [ 
        'options' => [ 
            'placeholder' => 'Pick Verification Time',
        ],
        'value' => date ( 'Y-M-d H:i:s'),
        'type' => DatePicker::TYPE_COMPONENT_PREPEND,
        'readonly' => true,
        'pluginOptions' => [ 
            'autoclose' => true,
            'format' => 'yyyy-mm-dd HH:ii:ss',
            'todayHighlight' => TRUE 
        ] 
    ] )->label ( false );

    ?>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
