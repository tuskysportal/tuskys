<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Outgoing */

$this->title = 'Create Outgoing';
$this->params['breadcrumbs'][] = ['label' => 'Outgoings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="outgoing-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
