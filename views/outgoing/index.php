<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\OutgoingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Outgoings';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="outgoing-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Outgoing', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'mobile',
            'msg:ntext',
            'dest',
            'ticket_id',
            //'created_at',
            //'state',
            //'acc_id',
            //'msg_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
