<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
Yii::setAlias('@css', '@web/css/');
Yii::setAlias('@images', '@web/images/');

$fieldOptions1 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-user form-control-feedback'></span>"
];
$fieldOptions2 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span>"
];


?>
<head>
    <link rel="stylesheet" href="<?= Yii::getAlias('@css') ?>/login.css">
    <style type="text/css">
      .form-control{
        width: 100%;
        padding: 0px;
        height: 35px;
        border: 1px solid #cccccc;
        box-sizing: border-box;
        outline: none;
        background-color: #fffafa00;
        border-radius: 0px;
      }
      .form-control:hover, .form-control:focus{
        background-color: #fffafa00;
      }
      .btn-primary{
        background: #f5ba1a;
        height: 35px;
        width: 100%;
        border: none;
        border-radius: 0px;
        outline: none;
        cursor: pointer;
        color: #fff;
        font-size: 1.1em;
        margin-bottom: 10px;
      }
      .btn-primary:hover,.btn-primary:focus{
        background-color: #f5ba1a;
        border-color: #f5ba1a;
      }
      .navbar-inverse {
        background-color: #165729;
        border-color: #165729;
      }
      .navbar-inverse .navbar-nav > .active > a, .navbar-inverse .navbar-nav > .active > a:hover, .navbar-inverse .navbar-nav > .active > a:focus {
          color: #fff;
          background-color: #165729;
      }
      .navbar-inverse .navbar-nav > li > a {
          color: #fff;
      }
      .navbar-inverse .navbar-brand {
          color: #fff;
      }
    </style>
</head>
<img src="<?= Yii::getAlias('@images') ?>/tuskys-logo.png" style="display: block;margin: 15% auto 0;width: 100%;max-width: 270px;height: auto" width="280" height="70" />
<div class="form_wrapper">         
  <div class="form_container">
    <div class="title_container">
      <h2>Tuskys Online Cashier</h2>
    </div>
    <div class="row clearfix">
      <div class="">

    <?php $form = ActiveForm::begin([
    ]); ?>

        <?= $form
      ->field($model, 'username', $fieldOptions1)
      ->label(false)
      ->textInput(['placeholder' => $model->getAttributeLabel('phone number')]) ?>

    <?= $form
      ->field($model, 'password', $fieldOptions2)
      ->label(false)
      ->passwordInput(['placeholder' => $model->getAttributeLabel('pin (4-digits)')]) ?>

        <div class="">
                <?= Html::submitButton('Login', ['class' => 'btn btn-primary btn-block btn-flat', 'name' => 'login-button']) ?>
        </div>

    <?php ActiveForm::end(); ?>
    
</div>
    </div>
  </div>
</div>
<script type="text/javascript" src="https://use.fontawesome.com/4ecc3dbb0b.js"></script>
