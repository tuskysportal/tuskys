<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DispatchSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dispatch-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'packagingId') ?>

    <?= $form->field($model, 'dispatch_agent') ?>

    <?= $form->field($model, 'dispatchStartTime') ?>

    <?= $form->field($model, 'dispatch_status') ?>

    <?php // echo $form->field($model, 'countOfPackagesDispatched') ?>

    <?php // echo $form->field($model, 'deliveryStatus') ?>

    <?php // echo $form->field($model, 'deliveryAgent') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'dispatchEndTime') ?>

    <?php // echo $form->field($model, 'deliveryStartTime') ?>

    <?php // echo $form->field($model, 'deliveryEndTime') ?>

    <?php // echo $form->field($model, 'collectedBy') ?>

    <?php // echo $form->field($model, 'collectorMobileNo') ?>

    <?php // echo $form->field($model, 'collectorIdNo') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
