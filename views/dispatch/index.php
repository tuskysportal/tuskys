<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\export\ExportMenu;
use app\models\Incoming;

/* @var $this yii\web\View */
/* @var $searchModel app\models\IncomingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Dispatch Unit';
$this->params['breadcrumbs'][] = $this->title;
?>
<head>
<style type="text/css">
    .panel-primary > .panel-heading {
    color: #fff;
    background-color: #465A6E;
    border-color: #465A6E;
    }
    .btn-primary {
        color: #fff;
        background-color: #465A6E;
        border-color: #465A6E;
        border-radius: 35px;
    }
    .pagination > .active > a, .pagination > .active > span, .pagination > .active > a:hover, .pagination > .active > span:hover, .pagination > .active > a:focus, .pagination > .active > span:focus {
        z-index: 3;
        color: #fff;
        cursor: default;
        background-color: #465A6E;
        border-color: #465A6E;
    }
    .red {
        color: white;
        background: green;
    }
    .btnstatus {
        border-radius: 35px;
        margin-bottom: 5px;
        margin-right: 5px;
        padding: 6px 12px;
        font-size: 14px;
        line-height: 1.42857143;
        touch-action: manipulation;
        cursor: pointer;
        background-image: none;
        border: 1px solid transparent;
        display: inline-block;
        font-weight: normal;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
    }
</style>
</head>
<div class="dispatch-index">
    <h3 style="padding-bottom: 5px;margin-top: -10px"><?= Html::encode($this->title) ?></h3>
    <p>
    <?php echo ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            
            //'id',
            'packagingId',
            'dispatch_status',
            'countOfPackagesDispatched',
            'collectorIdNo',
        ],
        
    ]); ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],

            //'id',
            'packagingId',
            //'dispatch_agent',
            //'dispatchStartTime',
            'dispatch_status',
            'countOfPackagesDispatched',
            //'deliveryStatus',
            //'deliveryAgent',
            //'created_at',
            //'dispatchEndTime',
            //'deliveryStartTime',
            //'deliveryEndTime',
            'collectedBy',
            'collectorMobileNo',
            'collectorIdNo',
            [
                'class' => 'kartik\grid\ActionColumn',
                'template' => '{view}{update}',
                'dropdown' => true,
                'vAlign'=>'middle',
            ],

            
        ],
        'containerOptions' => ['style'=>'overflow: auto'], 
        'toolbar' =>  [
            //'{export}',
            '{toggleData}'
        ],
        'pjax' => true,
        'bordered' => true,
        'striped' => false,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'floatHeader' => false,
        //'showPageSummary' => true,
        'rowOptions' => ['class' => ''],
        'headerRowOptions' => ['class' => ''],
        'layout' => "{items}\n{pager}",
        'panel' => [
            'type' => GridView::TYPE_PRIMARY
        ],
    ]); ?>


</div>
