<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Dispatch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dispatch-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'packagingId')->textInput(['maxlength' => true]) ?>

    <!-- <?= $form->field($model, 'dispatch_agent')->textInput(['maxlength' => true]) ?> -->

    <?= $form->field($model, 'dispatchStartTime')->textInput() ?>

    <?= $form->field($model, 'dispatch_status')->dropDownList([ 'Not_Dispatched' => 'Not Dispatched', 'Dispatch_In_Progress' => 'Dispatch In Progress', 'Dispatch_Completed' => 'Dispatch Completed', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'countOfPackagesDispatched')->textInput() ?>

    <?= $form->field($model, 'deliveryStatus')->dropDownList([ 'Not_Delivered' => 'Not Delivered', 'Delivery_In_Progress' => 'Delivery In Progress', 'Successfully_Delivered' => 'Successfully Delivered', 'Unsuccessfully_Delivered' => 'Unsuccessfully Delivered', ], ['prompt' => '']) ?>

 <!--    <?= $form->field($model, 'deliveryAgent')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'dispatchEndTime')->textInput() ?>

    <?= $form->field($model, 'deliveryStartTime')->textInput() ?>

    <?= $form->field($model, 'deliveryEndTime')->textInput() ?>
 -->
    <?= $form->field($model, 'collectedBy')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'collectorMobileNo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'collectorIdNo')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
