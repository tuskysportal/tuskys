<?php

/**
 * @var string $content
 * @var \yii\web\View $this
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\NavBar;
use yii\bootstrap\Nav;
use backend\models\User;

$bundle = yiister\gentelella\assets\Asset::register($this);

?>

<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta charset="<?= Yii::$app->charset ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="nav-<?= !empty($_COOKIE['menuIsCollapsed']) && $_COOKIE['menuIsCollapsed'] == 'true' ? 'sm' : 'md' ?>" >
<?php $this->beginBody(); ?>
<div class="container body">

    <div class="main_container">

        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">

                    <div class="navbar nav_title" style="border: 0;">
                        <a href="#" class="site_title"><i class="fa fa-shopping-cart"></i> <span>TUSKYS</span></a>
                    </div>
                    <div class="clearfix"></div>

                    <br />

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                    <div class="menu_section">
                        

                        <?=
                        \yiister\gentelella\widgets\Menu::widget(
                            [
                                "items" => [
                                    [
                                        "label" => "Tickets",
                                        "icon" => "phone",
                                        "url" => "#",
                                        "items" => [
                                            [
                                                "label" => "Active Tickets",
                                                "url" => ["incoming/index"],
                                            ],
                                            // [
                                            //     "label" => "Active Tickets",
                                            //     "url" => ["ticket/indexactive"],
                                            // ],
                                            [
                                                "label" => "Pending Tickets",
                                                "url" => ["ticket/index"],
                                            ],
                                            [
                                                "label" => "Closed Tickets",
                                                "url" => ["ticket/indexclosed"],
                                            ],
                                        ], 
                                    ],
                                    [
                                        "label" => "Shopping Lists",
                                        "icon" => "shopping-cart",
                                        "url" => "#",
                                        "items" => [
                                            [
                                                "label" => "Add An Order",
                                                "url" => ["orders/create"],
                                            ],
                                            [
                                                "label" => "View all Orders",
                                                "url" => ["orders/index"],
                                            ],
                                        ], 
                                    ],
                                    [
                                        "label" => "Payments",
                                        "icon" => "money",
                                        "url" => "#",
                                        "items" => [
                                            [
                                                "label" => "Confirmed Payments",
                                                "url" => ["#"],
                                            ],
                                        ], 
                                    ],
                                    [
                                        "label" => "Packaging",
                                        "icon" => "check",
                                        "url" => "#",
                                        "items" => [
                                            [
                                                "label" => "Process Package",
                                                "url" => ["packaging/create"],
                                            ],
                                            [
                                                "label" => "Confirm Packaging Status",
                                                "url" => ["packaging/index"],
                                            ],
                                        ], 
                                    ],
                                    [
                                        "label" => "Dispatch",
                                        "icon" => "check",
                                        "url" => "#",
                                        "items" => [
                                            [
                                                "label" => "All Dispatch",
                                                "url" => ["dispatch/index"],
                                            ],
                                        ], 
                                    ],
                                    
                                    
                                    
                    
                                ],
                            ]
                        )
                        ?> 
                       
                    </div>

                </div>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->
                <div class="sidebar-footer hidden-small">
                    <a data-toggle="tooltip" data-placement="top" title="Settings">
                        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                        <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Lock">
                        <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Logout">
                        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                    </a>
                </div>
                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">

        <?php
                    NavBar::begin([
                          //'brandLabel' => '', 
                          'options' => [
                          'class' => 'nav_menu',
                          ],
                    ]);
                    echo '<div class="nav toggle">
                              <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                          </div>';
                          echo Nav::widget([
                               'options' => ['class' => 'nav navbar-nav navbar-right'],
                               'items' => [
                                      Yii::$app->user->isGuest ?
                                          ['label' => 'Login', 'url' => ['/site/login']] :
                                              [
                                                  'label' => 'Logout (' . Yii::$app->user->identity->username . ')',
                                                  'url' => ['/site/logout'],
                                                  'linkOptions' => ['data-method' => 'post']
                                              ],

                                          ],

                          ]);
                    NavBar::end();
        ?>

        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <?php if (isset($this->params['h1'])): ?>
                <div class="page-title">
                    <div class="title_left">
                        <h1><?= $this->params['h1'] ?></h1>
                    </div>
                    <div class="title_right">
                        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search for...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">Go!</button>
                            </span>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <div class="clearfix"></div>

            <?= $content ?>
        </div>
        <!-- /page content -->
        <!-- footer content -->
        <footer>
            <div class="pull-left">
                Copyright &copy; 2020 Tuskys Supermarket.<a href="" rel="nofollow" target="_blank"> All rights reserved.</a>
            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>

</div>

<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>
<!-- /footer content -->
<?php $this->endBody(); ?>
</body>
</html>
<?php $this->endPage(); ?>

