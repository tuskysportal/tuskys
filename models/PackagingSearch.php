<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Packaging;

/**
 * PackagingSearch represents the model behind the search form of `app\models\Packaging`.
 */
class PackagingSearch extends Packaging
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'orderId', 'packaging_agent', 'Count_Of_Packages', 'verified_by'], 'integer'],
            [['packing_status', 'created_at', 'packing_start_time', 'packing_end_time', 'verification_time'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Packaging::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'orderId' => $this->orderId,
            'packaging_agent' => $this->packaging_agent,
            'Count_Of_Packages' => $this->Count_Of_Packages,
            'created_at' => $this->created_at,
            'packing_start_time' => $this->packing_start_time,
            'packing_end_time' => $this->packing_end_time,
            'verified_by' => $this->verified_by,
            'verification_time' => $this->verification_time,
        ]);

        $query->andFilterWhere(['like', 'packing_status', $this->packing_status]);

        return $dataProvider;
    }

    public function searchPackaging($params)
{
        $query = (new \yii\db\Query())
          ->select('tbl.id AS OrderId, tbl.agent AS PackagingAgent, tbl.mobile AS mobile, tbl.cnt AS PackageCount, tbl.stt AS Status, tbl.dte AS Date, tbl.verifier AS Verifier')
          ->from(["(SELECT p.orderId AS id,CONCAT((SELECT `firstname` FROM `users` WHERE `id`= p.`packaging_agent`),' ',
            (SELECT `lastname` FROM `users` WHERE `id`= p.`packaging_agent`)) AS `agent`,(SELECT `msisdn` FROM `users` WHERE id = p.`packaging_agent`) AS mobile, p.Count_Of_Packages AS cnt, p.packing_status AS stt, p.created_at AS dte,(SELECT `msisdn` FROM `users` WHERE id = p.`verified_by`) AS verifier
            FROM packaging p LEFT JOIN users u ON u.id = p.packaging_agent) AS tbl"]);

        $dataProvider = new ActiveDataProvider([ 
                'query' => $query,
                
        ]);
        $this->load($params);

        if (!$this->validate()) {
         
            return $dataProvider;
        }

        return $dataProvider;

    }
}
