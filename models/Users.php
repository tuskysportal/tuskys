<?php

namespace app\models;

use Yii;
use app\libraries\Encryption;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string|null $username
 * @property string|null $firstname
 * @property string|null $lastname
 * @property string|null $msisdn
 * @property string|null $repeat_password
 * @property string|null $name
 * @property string|null $email
 * @property string|null $password
 * @property string|null $salt
 * @property string|null $activation_key
 * @property int|null $status_id
 * @property string|null $dateCreated
 * @property int|null $created_by
 * @property string|null $dateModified
 * @property int|null $modified_by
 * @property int $role_id
 *
 * @property Role $role
 */
class Users extends ActiveRecord implements IdentityInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'name', 'email', 'password', 'salt', 'activation_key', 'dateCreated', 'dateModified'], 'string'],
            [['status_id', 'created_by', 'modified_by', 'role_id'], 'integer'],
            [['role_id'], 'required'],
            [['firstname', 'lastname'], 'string', 'max' => 50],
            [['msisdn'], 'string', 'max' => 12],
            [['repeat_password'], 'string', 'max' => 255],
            [['role_id'], 'exist', 'skipOnError' => true, 'targetClass' => Role::className(), 'targetAttribute' => ['role_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'firstname' => 'Firstname',
            'lastname' => 'Lastname',
            'msisdn' => 'Msisdn',
            'repeat_password' => 'Repeat Password',
            'name' => 'Name',
            'email' => 'Email',
            'password' => 'Password',
            'salt' => 'Salt',
            'activation_key' => 'Activation Key',
            'status_id' => 'Status ID',
            'dateCreated' => 'Date Created',
            'created_by' => 'Created By',
            'dateModified' => 'Date Modified',
            'modified_by' => 'Modified By',
            'role_id' => 'Role ID',
        ];
    }

    /**
     * Gets query for [[Role]].
     *
     * @return \yii\db\ActiveQuery
     */
    public static function findByUsername($username) {
        $user = Users::find()->where('username =:username', [':username' => $username])->one();
        if ($user)
            return $user;
        return null;
    }

    public function validatePassword($password, $goodhash, $salt) {
        return Encryption::validate_password($password, $goodhash, $salt);
    }

        public function getId() {
        return $this->id;
    }

    public function getAuthKey() {
        
    }

    public function validateAuthKey($authKey) {
        
    }

    public static function findIdentity($id) {
        return Users::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null) {
        
    }

    public function getRole()
    {
        return $this->hasOne(Role::className(), ['id' => 'role_id']);
    }
}
