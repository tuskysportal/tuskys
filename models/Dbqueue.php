<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dbqueue".
 *
 * @property int $id
 * @property string|null $originator
 * @property string|null $destination
 * @property string|null $message
 * @property string|null $scheduled_time
 * @property string $date_created
 */
class Dbqueue extends \yii\db\ActiveRecord
{
    const SENDERID='Tuskys';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dbqueue';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['message'], 'string'],
            [['scheduled_time', 'date_created'], 'safe'],
            [['originator', 'destination'], 'string', 'max' => 12],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'originator' => 'Originator',
            'destination' => 'Destination',
            'message' => 'Message',
            'scheduled_time' => 'Scheduled Time',
            'date_created' => 'Date Created',
        ];
    }
}
