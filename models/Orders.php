<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "orders".
 *
 * @property int $id
 * @property string $mobile
 * @property string $list
 * @property float $deliveryCost
 * @property float $totalCost
 * @property int $cashierId
 * @property string|null $location
 * @property string $deliveryModel
 * @property string $orderNumber
 * @property int|null $otp
 * @property int $ticketId
 * @property string $dateCreated
 * @property float $outstandingBalance
 * @property string|null $paidUpStatus
 * @property string|null $scheduleCollectionTime
 *
 * @property Ticket $ticket
 * @property Users $cashier
 * @property Packaging[] $packagings
 * @property Payments[] $payments
 */
class Orders extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'orders';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['mobile', 'list', 'deliveryCost', 'totalCost', 'cashierId', 'orderNumber', 'ticketId'], 'required'],
            [['list', 'deliveryModel', 'paidUpStatus'], 'string'],
            [['deliveryCost', 'totalCost', 'outstandingBalance'], 'number'],
            [['cashierId', 'otp', 'ticketId'], 'integer'],
            [['dateCreated', 'scheduleCollectionTime'], 'safe'],
            [['mobile'], 'string', 'max' => 12],
            [['location'], 'string', 'max' => 1000],
            [['orderNumber'], 'string', 'max' => 255],
            [['ticketId'], 'exist', 'skipOnError' => true, 'targetClass' => Ticket::className(), 'targetAttribute' => ['ticketId' => 'id']],
            [['cashierId'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['cashierId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mobile' => 'Mobile',
            'list' => 'List',
            'deliveryCost' => 'Delivery Cost',
            'totalCost' => 'Total Cost',
            'cashierId' => 'Cashier ID',
            'location' => 'Location',
            'deliveryModel' => 'Delivery Model',
            'orderNumber' => 'Order Number',
            'otp' => 'Otp',
            'ticketId' => 'Ticket ID',
            'dateCreated' => 'Date Created',
            'outstandingBalance' => 'Outstanding Balance',
            'paidUpStatus' => 'Paid Up Status',
            'scheduleCollectionTime' => 'Schedule Collection Time',
        ];
    }

    /**
     * Gets query for [[Ticket]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTicket()
    {
        return $this->hasOne(Ticket::className(), ['id' => 'ticketId']);
    }

    /**
     * Gets query for [[Cashier]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCashier()
    {
        return $this->hasOne(Users::className(), ['id' => 'cashierId']);
    }

    /**
     * Gets query for [[Packagings]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPackagings()
    {
        return $this->hasMany(Packaging::className(), ['orderId' => 'id']);
    }

    /**
     * Gets query for [[Payments]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPayments()
    {
        return $this->hasMany(Payments::className(), ['orderId' => 'id']);
    }
}
