<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tickets".
 *
 * @property int $id
 * @property string|null $mobile
 * @property string|null $date_created
 * @property string|null $status
 * @property string $ticket_status
 * @property int|null $user_id
 */
class Tickets extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tickets';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date_created'], 'safe'],
            [['ticket_status'], 'required'],
            [['ticket_status'], 'string'],
            [['user_id'], 'integer'],
            [['mobile'], 'string', 'max' => 15],
            [['status'], 'string', 'max' => 1],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mobile' => 'Mobile',
            'date_created' => 'Date Created',
            'status' => 'Status',
            'ticket_status' => 'Ticket Status',
            'user_id' => 'User ID',
        ];
    }
}
