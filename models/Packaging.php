<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "packaging".
 *
 * @property int $id
 * @property int $orderId
 * @property int|null $packaging_agent
 * @property int $Count_Of_Packages
 * @property string $packing_status
 * @property string $created_at
 * @property string|null $packing_start_time
 * @property string|null $packing_end_time
 * @property int|null $verified_by
 * @property string|null $verification_time
 *
 * @property Dispatch[] $dispatches
 * @property Orders $order
 * @property Users $packagingAgent
 * @property Users $verifiedBy
 */
class Packaging extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'packaging';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['orderId'], 'required'],
            [['orderId', 'packaging_agent', 'Count_Of_Packages', 'verified_by'], 'integer'],
            [['packing_status'], 'string'],
            [['created_at', 'packing_start_time', 'packing_end_time', 'verification_time'], 'safe'],
            [['orderId'], 'exist', 'skipOnError' => true, 'targetClass' => Orders::className(), 'targetAttribute' => ['orderId' => 'id']],
            [['packaging_agent'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['packaging_agent' => 'id']],
            [['verified_by'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['verified_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'orderId' => 'Order ID',
            'packaging_agent' => 'Packaging Agent',
            'Count_Of_Packages' => 'Count Of Packages',
            'packing_status' => 'Packing Status',
            'created_at' => 'Created At',
            'packing_start_time' => 'Packing Start Time',
            'packing_end_time' => 'Packing End Time',
            'verified_by' => 'Verified By',
            'verification_time' => 'Verification Time',
        ];
    }

    /**
     * Gets query for [[Dispatches]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDispatches()
    {
        return $this->hasMany(Dispatch::className(), ['packagingId' => 'id']);
    }

    /**
     * Gets query for [[Order]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Orders::className(), ['id' => 'orderId']);
    }

    /**
     * Gets query for [[PackagingAgent]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPackagingAgent()
    {
        return $this->hasOne(Users::className(), ['id' => 'packaging_agent']);
    }

    /**
     * Gets query for [[VerifiedBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVerifiedBy()
    {
        return $this->hasOne(Users::className(), ['id' => 'verified_by']);
    }
}
