<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dispatch".
 *
 * @property int $id
 * @property int $packagingId
 * @property int|null $dispatch_agent
 * @property string|null $dispatchStartTime
 * @property string $dispatch_status
 * @property int $countOfPackagesDispatched
 * @property string $deliveryStatus
 * @property int|null $deliveryAgent
 * @property string $created_at
 * @property string|null $dispatchEndTime
 * @property string|null $deliveryStartTime
 * @property string|null $deliveryEndTime
 * @property string|null $collectedBy
 * @property string|null $collectorMobileNo
 * @property string|null $collectorIdNo
 *
 * @property Packaging $packaging
 * @property Users $dispatchAgent
 * @property Users $deliveryAgent0
 */
class Dispatch extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dispatch';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['packagingId'], 'required'],
            [['packagingId', 'dispatch_agent', 'countOfPackagesDispatched', 'deliveryAgent'], 'integer'],
            [['dispatchStartTime', 'created_at', 'dispatchEndTime', 'deliveryStartTime', 'deliveryEndTime'], 'safe'],
            [['dispatch_status', 'deliveryStatus'], 'string'],
            [['collectedBy'], 'string', 'max' => 100],
            [['collectorMobileNo', 'collectorIdNo'], 'string', 'max' => 15],
            [['packagingId'], 'exist', 'skipOnError' => true, 'targetClass' => Packaging::className(), 'targetAttribute' => ['packagingId' => 'id']],
            [['dispatch_agent'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['dispatch_agent' => 'id']],
            [['deliveryAgent'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['deliveryAgent' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'packagingId' => 'Packaging ID',
            'dispatch_agent' => 'Dispatch Agent',
            'dispatchStartTime' => 'Dispatch Start Time',
            'dispatch_status' => 'Dispatch Status',
            'countOfPackagesDispatched' => 'Count Of Packages Dispatched',
            'deliveryStatus' => 'Delivery Status',
            'deliveryAgent' => 'Delivery Agent',
            'created_at' => 'Created At',
            'dispatchEndTime' => 'Dispatch End Time',
            'deliveryStartTime' => 'Delivery Start Time',
            'deliveryEndTime' => 'Delivery End Time',
            'collectedBy' => 'Collected By',
            'collectorMobileNo' => 'Collector Mobile No',
            'collectorIdNo' => 'Collector Id No',
        ];
    }

    /**
     * Gets query for [[Packaging]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPackaging()
    {
        return $this->hasOne(Packaging::className(), ['id' => 'packagingId']);
    }

    /**
     * Gets query for [[DispatchAgent]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDispatchAgent()
    {
        return $this->hasOne(Users::className(), ['id' => 'dispatch_agent']);
    }

    /**
     * Gets query for [[DeliveryAgent0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveryAgent0()
    {
        return $this->hasOne(Users::className(), ['id' => 'deliveryAgent']);
    }
}
