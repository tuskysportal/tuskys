<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "outgoing".
 *
 * @property int $id
 * @property string|null $mobile
 * @property string|null $msg
 * @property string|null $dest
 * @property int|null $ticket_id
 * @property string|null $created_at
 * @property resource|null $state
 * @property string|null $acc_id
 * @property string|null $msg_id
 */
class Outgoing extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'outgoing';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['msg'], 'string'],
            [['ticket_id'], 'integer'],
            [['created_at'], 'safe'],
            [['mobile'], 'string', 'max' => 15],
            [['dest', 'acc_id', 'msg_id'], 'string', 'max' => 50],
            [['state'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mobile' => 'Mobile',
            'msg' => 'Msg',
            'dest' => 'Dest',
            'ticket_id' => 'Ticket ID',
            'created_at' => 'Created At',
            'state' => 'State',
            'acc_id' => 'Acc ID',
            'msg_id' => 'Msg ID',
        ];
    }
}
