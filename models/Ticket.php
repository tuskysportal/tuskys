<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ticket".
 *
 * @property int $id
 * @property string|null $mobile
 * @property string|null $date_created
 * @property string|null $status
 * @property string|null $ticket_status
 * @property int|null $user_id
 *
 * @property Incoming[] $incomings
 * @property Orders[] $orders
 * @property Outgoing[] $outgoings
 * @property Users $user
 */
class Ticket extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ticket';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date_created'], 'safe'],
            [['ticket_status'], 'string'],
            [['user_id'], 'integer'],
            [['mobile'], 'string', 'max' => 15],
            [['status'], 'string', 'max' => 1],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mobile' => 'Mobile',
            'date_created' => 'Date Created',
            'status' => 'Status',
            'ticket_status' => 'Ticket Status',
            'user_id' => 'User ID',
        ];
    }

    /**
     * Gets query for [[Incomings]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIncomings()
    {
        return $this->hasMany(Incoming::className(), ['ticket_id' => 'id']);
    }

    /**
     * Gets query for [[Orders]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Orders::className(), ['ticketId' => 'id']);
    }

    /**
     * Gets query for [[Outgoings]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOutgoings()
    {
        return $this->hasMany(Outgoing::className(), ['ticket_id' => 'id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

        public static function get_user_message($mobile){
        $model = Incoming::find()->where(["mobile" => $mobile])->one();
        if(!empty($model)){
            return $model->msg;
        }

        return null;
    }

    public static function get_cashier_reply($mobile){
        $model = Outgoing::find()->where(["dest" => $mobile])->one();
        if(!empty($model)){
            return $model->msg;
        }

        return null;
    }


    public function incoming(){
        $query = Yii::$app->db->createCommand("SELECT i.`msg` FROM `ticket` AS t INNER JOIN `incoming` AS i ON t.`id`=i.`ticket_id` WHERE t.`mobile`=254745725567 AND t.`ticket_status`='Pending'")
            ->execute();
    }

    public function outgoing(){

        if (isset($_POST) && !empty($_POST)) {

            $data = $_POST;
            $chat_message =  $data['chat_message'];
            $mobile = 14155238886;
            $destination = 254701405514;
            $ticketid = 4;
            $msg_id = 5;
            $state = "ticket";

            var_dump($data);die();

            
            Yii::$app->db->createCommand("INSERT INTO outgoing (mobile,msg,dest,ticket_id,state,msg_id) VALUES ('".$mobile."','".$chat_message."','".$destination."','".$ticketid."','".$state."','".$msg_id."') ")
            ->execute();

            return true;

        }
        return false;

        // var_dump($query);
        // die();
    }
}
