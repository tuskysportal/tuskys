<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Dispatch;

/**
 * DispatchSearch represents the model behind the search form of `app\models\Dispatch`.
 */
class DispatchSearch extends Dispatch
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'packagingId', 'dispatch_agent', 'countOfPackagesDispatched', 'deliveryAgent'], 'integer'],
            [['dispatchStartTime', 'dispatch_status', 'deliveryStatus', 'created_at', 'dispatchEndTime', 'deliveryStartTime', 'deliveryEndTime', 'collectedBy', 'collectorMobileNo', 'collectorIdNo'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Dispatch::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'packagingId' => $this->packagingId,
            'dispatch_agent' => $this->dispatch_agent,
            'dispatchStartTime' => $this->dispatchStartTime,
            'countOfPackagesDispatched' => $this->countOfPackagesDispatched,
            'deliveryAgent' => $this->deliveryAgent,
            'created_at' => $this->created_at,
            'dispatchEndTime' => $this->dispatchEndTime,
            'deliveryStartTime' => $this->deliveryStartTime,
            'deliveryEndTime' => $this->deliveryEndTime,
        ]);

        $query->andFilterWhere(['like', 'dispatch_status', $this->dispatch_status])
            ->andFilterWhere(['like', 'deliveryStatus', $this->deliveryStatus])
            ->andFilterWhere(['like', 'collectedBy', $this->collectedBy])
            ->andFilterWhere(['like', 'collectorMobileNo', $this->collectorMobileNo])
            ->andFilterWhere(['like', 'collectorIdNo', $this->collectorIdNo]);

        return $dataProvider;
    }
}
