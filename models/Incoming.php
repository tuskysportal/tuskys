<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "incoming".
 *
 * @property int $id
 * @property string $mobile
 * @property string|null $msg
 * @property string $dest
 * @property int|null $ticket_id
 * @property string|null $created_at
 * @property string|null $state
 * @property string|null $acc_id
 */
class Incoming extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $reply;

    public static function tableName()
    {
        return 'incoming';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['mobile', 'dest'], 'required'],
            [['msg'], 'string'],
            [['ticket_id'], 'integer'],
            [['created_at','reply'], 'safe'],
            [['mobile', 'dest'], 'string', 'max' => 15],
            [['state'], 'string', 'max' => 10],
            [['acc_id'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mobile' => 'Phone Number',
            'msg' => 'Message',
            'dest' => 'Destination WhatsApp Number',
            'ticket_id' => 'Ticket ID',
            'created_at' => 'Created At',
            'state' => 'State',
            'acc_id' => 'Acc ID',
        ];
    }

    public static function get_active_cashier_reply($mobile,$id){
        $model = Outgoing::find()->where(["dest" => $mobile])->andwhere(["msg_id"=>$id])->one();
        if(!empty($model)){
            return $model->msg;
        }

        return null;
    }
}
